import Vue from 'vue'
import Router from 'vue-router'
import AuthGuard from './auth-guard'
import Authentification from './views/Authentification.vue'
import Annees from './views/Annees.vue'
import Ecole from './views/Ecole.vue'
import Etablissement from './views/Etablissement.vue'
import Classe from './components/Classe.vue'
import Eleves from './components/Eleves.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
            path: '/',
            name: 'authentification',
            component: Authentification
        },
        {
            path: '/annees',
            name: 'annees',
            component: Annees,
            beforeEnter: AuthGuard
        },
        {
            path: '/ecole',
            name: 'ecole',
            component: Ecole
        },
        {
            path: '/etablissement',
            name: 'etablissement',
            component: Etablissement
        },
        {
            path: '/classe',
            name: 'classe',
            component: Classe
        },
        {
            path: '/eleves',
            name: 'eleves',
            component: Eleves
        }
    ]
})