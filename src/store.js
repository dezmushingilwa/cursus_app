import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token: '',
        annee: '',
        ecole: '',
        section: '',
        classe: ''
    },
    mutations: {
        setToken(state, p) {
            state.token = p.token;
        },
        setAnnee(state, a) {
            state.annee = a.annee;
        },
        setEcole(state, e) {
            state.ecole = e.ecole;
        },
        setSection(state, s) {
            state.section = s.section
        },
        setClasse(state, c) {
            state.classe = c.classe
        }
    },
    actions: {

    },
    getters: {
        getToken(state) {
            return state.token;
        },
        getAnnee(state) {
            return state.annee;
        },
        getEcole(state) {
            return state.ecole;
        },
        getSection(state) {
            return state.section;
        },
        getClasse(state) {
            return state.classe;
        }
    }
})